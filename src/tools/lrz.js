function imgUrl(file) {
    return new Promise(async (res, rej) => {
        let reader = new FileReader()
        if (file.type.indexOf("image") === 0) {
            reader.readAsDataURL(file);
        } else {
            rej('请上传图片文件~~')
        }
        reader.onload = function (e) {
            res(e.target.result)
        }
    }).catch((err) => {
        return err
    })
}

async function lrzImg(file) {
    let op = new Promise(async (resolve, reject) => {
        // let reader = new FileReader(),
           let img = new Image();

        // if (file.type.indexOf("image") === 0) {
        //     reader.readAsDataURL(file);
        // } else {
        //     reject('请上传图片文件~~')
        // }
        // reader.onload = function (e) {
        //     img.src = e.target.result;
        // }
        let imageUrl = await imgUrl(file)
        if (typeof imageUrl === 'string') {
            reject(imageUrl)
        }
        img.src = imageUrl
        let img64 = await returnImg(img, file.name, file.type)
        if (img64) resolve(img64)
    }).catch((err) => {
        return err
    })
    return op
}

function dataURLtoBlob(dataurl) {
    let arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type: mime});
}

function blobToFile(blob, fileName) {
    blob.lastModifiedDate = new Date();
    blob.name = fileName;
    return blob;
}

// function dataURLtoFile(dataurl, filename){
//     let arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
//         bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
//     while(n--){
//         u8arr[n] = bstr.charCodeAt(n);
//     }
//     let blob = dataURLtoBlob(dataurl);
//     return blobToFile(blob, filename);
// }

function returnImg(img, fileName, fileType) {
    let op = new Promise((resolve) => {
        let canvas = document.createElement('canvas');
        let context = canvas.getContext('2d');
        img.onload = function () {
            let originWidth = this.width;
            let originHeight = this.height;
            window.console.log(originWidth, originHeight)
            let maxWidth = 1500,
                maxHeight = 1500;
            let targetWidth = originWidth,
                targetHeight = originHeight;

            if (originWidth > maxWidth || originHeight > maxHeight) {
                if (originWidth / originHeight > maxWidth / maxHeight) {

                    targetWidth = maxWidth;
                    targetHeight = Math.round(maxWidth * (originHeight / originWidth));
                } else {
                    targetHeight = maxHeight;
                    targetWidth = Math.round(maxHeight * (originWidth / originHeight));
                }
            }
            canvas.width = targetWidth;
            canvas.height = targetHeight;
            context.clearRect(0, 0, targetWidth, targetHeight);
            context.drawImage(img, 0, 0, targetWidth, targetHeight);
            let blob = dataURLtoBlob(canvas.toDataURL(fileType || 'image/jpeg', 0.9))
            let file = blobToFile(blob, fileName)
            resolve(file)
            // canvas.toBlob((blob) => {
            //     // console.log(blob)
            //     let file = blobToFile(blob, fileName)
            //     resolve(file)
            // }, fileType || 'image/jpeg')
            // canvas.toDataURL()
            // let b = dataURLtoBlob(canvas.toDataURL('image/jpeg', 0.3))
            // console.log(b)
            // resolve(canvas.toDataURL('image/jpeg', 0.3))
        }
    }).catch((err) => {
        return err
    })
    return op
}

export default {imgUrl, lrzImg}
