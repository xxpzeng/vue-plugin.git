import Vue from 'vue'
import App from './App.vue'
import rem from '../rem'
import router from './router'
import toast from './myPlugin/toast'
import confirm from './myPlugin/confirm'
import axios from 'axios'
import markdown from 'mavon-editor'
import './assets/css/base.css'
import 'vue2-animate/dist/vue2-animate.min.css'
import 'mavon-editor/dist/css/index.css'

rem(document);
Vue.use(confirm)
Vue.use(toast())
Vue.use(markdown)
Vue.config.productionTip = false
Vue.prototype.$request = axios
// axios.defaults.baseURL = 'http://127.0.0.1:1234'
axios.interceptors.request.use(
    config => {
        // const token = localStorage.getItem('ybrToken')
        // if (config.url == 'http://www.yibangman.com/MillionHelp2/api/upload/FileUploadBlob') {
        //     config.headers = {
        //         'Content-Type': null
        //     }
        // }
        // if (token) {
        //     config.headers['Authorization'] = token
        // }
        return config
    },
    (error) => {
        // 对请求错误做些什么
        // return Promise.reject(error)
        window.console.log(error)
    }
)
// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    if (localStorage.getItem('errToken')) {
        localStorage.removeItem('errToken')
    }
    return response
}, function (error) {
    // if (error.response && error.response.data.message !== '验证码错误') {
    //     switch (error.response.status) {
    //         case 401:
    //             // 返回 401 清除token信息并跳转到登录页面
    //             // localStorage.removeItem('ybrToken')
    //             if (!localStorage.getItem('errToken')) {
    //                 localStorage.setItem('errToken', 'err')
    //             }
    //     }
    // }
    window.console.log(error)
    return error.response.data
})
new Vue({
    render: h => h(App),
    router
}).$mount('#app')
